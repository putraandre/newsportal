import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import detail from '@/components/detail'
import finance from '@/components/finance'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: 'detail',
      name: 'detail',
      component: detail
    },
    {
      path: 'finance',
      name: 'finance',
      component: finance
    }
  ]
})
